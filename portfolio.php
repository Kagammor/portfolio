<?php include('./header.php'); ?>
<div id="banner">
	<h1><span class="head_label">&#167;</span> Portfolio</h1>
</div>

<div id="portfolio">
	<div class="portfolio_item">
		<div class="portfolio_item_img">
			<a class="fancybox" href="./assets/img/portfolio/cigar.png" title="Cigar"><img src="./assets/img/portfolio/cigar.png" alt="Cigar"></a>
		</div>
		<h3>Cigar</h3>
		<p>Logo designed for a mobile app that tracks smoking habits.</p>
	</div>
	<div class="portfolio_item">
		<div class="portfolio_item_img">
			<a class="fancybox" href="./assets/img/portfolio/cadenza.png" title="Cadenza"><img src="./assets/img/portfolio/cadenza.png" alt="Cadenza"></a>
		</div>
		<h3>Cadenza</h3>
		<p>Colourful minimal iconset designed for a theatre application.</p>
	</div>
	<div class="portfolio_item">
		<div class="portfolio_item_img">
			<a class="fancybox" href="./assets/img/portfolio/creature1.jpg" title="Creature 1"><img src="./assets/img/portfolio/creature1_thumb.jpg" alt="Creature 1"></a>
		</div>
		<h3>Creature 1</h3>
		<p>Aquatic creature designed for personal exercise.</p>
	</div>
	<div class="portfolio_item">
		<div class="portfolio_item_img">
			<a class="fancybox" href="./assets/img/portfolio/creature2.jpg" title="Creature 2"><img src="./assets/img/portfolio/creature2_thumb.jpg" alt="Creature 2"></a>
		</div>
		<h3>Creature 2</h3>
		<p>Aquatic creature designed for personal exercise.</p>
	</div>
	
	<div style="clear: both;"></div>
</div>
<?php include('./footer.php'); ?>
