<!doctype html>
<html>
	<head>
		<title>Niels Simenon</title>
		
		<link href="./assets/css/reset.css" rel="stylesheet" type="text/css">
		<link href="./assets/css/style.css" rel="stylesheet" type="text/css">
		<link href="./assets/css/jquery.fancybox.css" rel="stylesheet" type="text/css">
		
		<script src="./assets/js/jquery.js"></script>
		<script src="./assets/js/jquery.fancybox.js"></script>
		
		<script type="text/javascript">
			$(document).ready(function() {
				$(".fancybox").fancybox({
					openEffect  : 'none',
					closeEffect	: 'none',

					helpers : {
						title : {
							type : 'inline'
						}
					}
				});
			});
		</script>
	</head>
	
	<body>
		<div class="wrapper">
			<header>
				<a href="/"><h1 id="logo">Niels Simenon</h1></a>
			
				<nav>
					<ul>
						<li><a href="./portfolio">portfolio</a></li>
						<li><a href="./webhosting">webhosting</a></li>
						<li><a href="./contact">contact</a></li>
					</ul>
				</nav>
			</header>
