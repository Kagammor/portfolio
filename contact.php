<?php include('./header.php'); ?>
<div id="banner">
	<h1><span class="head_label">&#167;</span> Contact</h1>
</div>

<div id="content">
	<ul id="contact">
		<li>
			<ul>
				<li style="font-weight: bold;">
					E-mail:
					<div class="social_icons" id="social_icons_hangouts"></div>
					<div class="social_icons" id="social_icons_gmail"></div>
				</li>
				<li><a href="mailto:niels.simenon@gmail.com">niels.simenon@gmail.com</a></li>
			</ul>
			<ul>
				<li style="font-weight: bold;">
					Phone:
					<div class="social_icons" id="social_icons_telegram"></div>
					<div class="social_icons" id="social_icons_whatsapp"></div>
				</li>
				<li><a href="callto:0031683548215">+31 6 8354 8215</a></li>
				<li style="color: #aaa; font-size: 0.75em; font-style: italic;">Please text or e-mail before calling.</li>
			</ul>
			<ul>
				<li style="font-weight: bold;">
					Skype:
					<div class="social_icons" id="social_icons_kik"></div>
					<div class="social_icons" id="social_icons_skype"></div>
				</li>
				<li><a href="skype:Kagammor">Kagammor</a></li>
				<li style="color: #aaa; font-size: 0.75em; font-style: italic;">Please chat before calling.</li>
			</ul>
		</li>
	</ul>
</div>
<?php include('./footer.php'); ?>
