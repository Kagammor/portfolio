<?php include('./header.php'); ?>
	<div id="banner">
		<h1>Hey! I'm Niels Simenon.</h1>
	</div>

	<div id="services">
		<div class="service_wrap">
			<div class="service" id="webdev">
				<div class="service_icon" id="service_icon_webdev"></div>
			
				<h2>Web Development</h2>
		
				<p class="service_ad">What a relief.</p>
				<p class="service_desc">Well developed websites are essential to anyone who has something to show the world.</p>
		
				<button>More about websites</button>
			</div>
		</div>
		<div class="service_wrap">
			<div class="service" id="design">
				<div class="service_icon" id="service_icon_design"></div>
			
				<h2>Graphic Design</h2>
		
				<p class="service_ad">Preeeetty!</p>
				<p class="service_desc">A first impression is about as important as the quality of your product or content.</p>
		
				<button>More about graphics</button>
			</div>
		</div>
		<div class="service_wrap">
			<div class="service" id="hosting">
				<div class="service_icon" id="service_icon_hosting"></div>
			
				<h2>Web Hosting</h2>
		
				<p class="service_ad">This webpage is available.</p>
				<p class="service_desc">Beautiful establishments can't go without a flexible and high-availability platform.</p>
		
				<button>More about hosting</button>
			</div>
		</div>
	
		<div style="clear: both;"></div>
	</div>
<?php include('./footer.php'); ?>
